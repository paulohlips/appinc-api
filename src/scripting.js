import Sequelize from "sequelize";

let json_model = [];
let json_insomnia = [];

const json = {
  pop_name: {
    type: Sequelize.STRING,
    allowNull: false,
  },
  pop_version: {
    type: Sequelize.STRING,
    allowNull: false,
  },
  pop_data: {
    type: Sequelize.STRING,
    allowNull: false,
  },
  area: {
    type: Sequelize.STRING,
    allowNull: false,
  },
  class: {
    type: Sequelize.STRING,
    allowNull: false,
  },
  subclass: {
    type: Sequelize.STRING,
    allowNull: false,
  },
  status: {
    type: Sequelize.BOOLEAN,
    defaultValue: true,
  },
  name: {
    type: Sequelize.STRING,
    allowNull: false,
  },
};

let count = 0;

Object.entries(json).forEach(async (item) => {
  let obj = `${Object.keys(json)[count]}: Sequelize.${json.name.type.key}`;
  json_model.push(obj);
  let obj2 = `"${Object.keys(json)[count]}": ""`;
  json_insomnia.push(obj2);
  count++;
});

console.log(json_model);
console.log(json_insomnia);

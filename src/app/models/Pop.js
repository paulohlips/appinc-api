import Sequelize, { Model } from "sequelize";

class Pop extends Model {
  static init(sequelize) {
    super.init(
      {
        name: Sequelize.STRING,
        version: Sequelize.STRING,
        area: Sequelize.STRING,
        class: Sequelize.STRING,
        subclass: Sequelize.STRING,
        status: Sequelize.BOOLEAN,
        details: Sequelize.STRING,
        data: Sequelize.ARRAY(Sequelize.JSON),
      },
      {
        sequelize,
      }
    );

    return this;
  }
}

export default Pop;

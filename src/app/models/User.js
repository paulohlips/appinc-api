import Sequelize, { Model } from "sequelize";
import bcrypt from "bcrypt";

class User extends Model {
  static init(sequelize) {
    super.init(
      {
        name: Sequelize.STRING,
        email: Sequelize.STRING,
        register: Sequelize.INTEGER,
        status: Sequelize.BOOLEAN,
        password: Sequelize.VIRTUAL,
        password_hash: Sequelize.STRING,
        pin: Sequelize.INTEGER,
      },
      {
        sequelize,
      }
    );

    return this;
  }

  checkpassword(password) {
    return bcrypt.compare(password, this.password_hash);
  }
}

export default User;

import jwt from "jsonwebtoken";
import * as Yup from "yup";
import writeLog from "../../utils/logger";
import sessionLogger from "../../utils/sessionLogger";

import User from "../models/User";

import auth from "../../config/auth";

class SessionController {
  async store(req, res) {
    const schema = Yup.object().shape({
      register: Yup.number().required(),
      password: Yup.string().required(),
    });

    if (!(await schema.isValid(req.body))) {
      return res.status(422).json({
        message: "Erro de validação. Verifique o body da requisição.",
      });
    }

    const { register, password } = req.body;

    try {
      const user = await User.findOne({
        where: {
          register,
        },
      });

      if (!user) {
        return res.status(404).json({ message: "Perito não encontrado." });
      }

      if (!(await user.checkpassword(password))) {
        return res.status(401).json({ message: "A password incorreta." });
      }

      const { id, name } = user;

      sessionLogger(id, name, register);

      return res.json({
        user: {
          id,
          register,
          name,
        },
        token: jwt.sign({ id }, auth.secret, {
          expiresIn: auth.expiresIn,
        }),
      });
    } catch (err) {
      writeLog(err);
      return res.status(500).json({ message: `${err}` });
    }
  }
}

export default new SessionController();

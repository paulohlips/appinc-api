import User from "../models/User";
import * as Yup from "yup";
import writeLog from "../../utils/logger";

class UserController {
  async store(req, res) {
    const schema = Yup.object().shape({
      register: Yup.number().required(),
    });

    if (!(await schema.isValid(req.body))) {
      return res.status(422).json({
        message: "Erro de validação. Verifique o body da requisição.",
      });
    }

    if (!(req.userType === "admin")) {
      return res.status(403).json({
        message:
          "Operação não permitida, o usuário não tem permissão de administrador",
      });
    }

    try {
      const { email } = req.body;

      const userExists = await User.findOne({ where: { email } });

      if (userExists) {
        return res
          .status(422)
          .json({ message: "O usuário já está cadastrado." });
      }

      const user = await User.create(req.body);

      return res.json(user);
    } catch (err) {
      writeLog(err);
      return res.status(500).json({ message: `${err}` });
    }
  }

  async update(req, res) {
    const { email } = req.body;

    try {
      User.update(req.body, { where: { email } });

      return res.json({ message: "Atualização realizada com sucesso." });
    } catch (err) {
      writeLog(err);
      return res.status(500).json({ message: `${err}` });
    }
  }

  async index(req, res) {
    try {
      const user = await User.findAll();

      return res.json(user);
    } catch (err) {
      writeLog(err);
      return res.status(500).json({ message: `${err}` });
    }
  }
}

export default new UserController();

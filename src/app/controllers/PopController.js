import Pop from "../models/Pop";
import knex from "../../database/knex";
import * as Yup from "yup";
import writeLog from "../../utils/logger";

class PopController {
  async store(req, res) {
    const schema = Yup.object().shape({
      name: Yup.string().required(),
      version: Yup.string().required(),
      area: Yup.string().required(),
      class: Yup.string().required(),
      subclass: Yup.string().required(),
      details: Yup.string().required(),
      data: Yup.array().required(),
    });

    if (!(await schema.isValid(req.body))) {
      return res.status(422).json({
        message: "Erro de validação. Verifique o body da requisição.",
      });
    }

    try {
      const { name, version, data } = req.body;
      let schema = [];
      let pop = {};

      const arr = data.map((item) => item.fields);

      for (let i = 0; i < arr.length; i++) {
        arr[i].map((item) => {
          schema.push({ name: item.key, type: item.type });
          console.log(arr.length, schema);
        });
      }

      const findPop = await Pop.findOne({
        where: { name },
      });

      if (findPop) {
        return res
          .status(400)
          .json({ message: `POP ${name} has already been registered` });
      }

      pop = await Pop.create(req.body);

      await knex.schema.createTable(`${name}`, function (table) {
        table.increments();
        schema.forEach((element) => {
          switch (element.type) {
            case "text":
              table.string(`${element.name}`);
              break;
            case "geoloc":
              table.string(`${element.name}`);
              break;
            case "camera":
              table.json(`${element.name}`);
              break;
            case "date":
              table.timestamp(`${element.name}`);
              break;
            default:
              break;
          }
        });
        table.timestamp("created_at").defaultTo(knex.fn.now());
        table.timestamp("updated_at").defaultTo(knex.fn.now());
      });

      return res.json(pop);
    } catch (err) {
      writeLog(err);
      return res.status(500).json({ message: `${err}` });
    }
  }

  async show(req, res) {
    const { id } = req.params;

    try {
      const pop = await Pop.findAll({ where: { id } });

      return res.json(pop);
    } catch (err) {
      writeLog(err);
      return res.status(500).json({ message: `${err}` });
    }
  }
}

export default new PopController();

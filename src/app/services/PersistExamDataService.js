import knex from "../../database/knex";

import writeLog from "../../utils/logger";
import Pop from "../models/Pop";
import File from "../models/File";

class PersistExamDataService {
  async store(req, res) {
    const { exam_type } = req.params;

    try {
      const examExists = await Pop.findOne({
        where: { name: exam_type },
      });

      if (!examExists) {
        return res.status(422).json({
          message: `Exam ${exam_type} is invalid`,
        });
      }

      Object.keys(req.body).forEach(async (item) => {
        if (!item.search("imagem_")) {
          const values = Object.values(req.body[item]);
          const name = values[1],
            path = values[1];

          const file = await File.create({
            user_id: 1,
            name,
            path,
          });
          return res.json({ file });
        }
      });

      //const exam = await knex("pericia_veiculo").insert(req.body);

      return res.json("exam");
    } catch (error) {
      return res.json({
        message: error,
      });
    }
  }
  catch(err) {
    writeLog(err);
    return res.status(500).json({ message: `${err}` });
  }
}

export default new PersistExamDataService();

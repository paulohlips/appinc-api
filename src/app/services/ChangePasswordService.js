import User from "../models/User";
import * as Yup from "yup";
import bcrypt from "bcrypt";
import writeLog from "../../utils/logger";

class ChangePasswordService {
  async store(req, res) {
    const schema = Yup.object().shape({
      password: Yup.string().required(),
      register: Yup.number().required(),
      pin: Yup.number().required(),
    });

    if (!(await schema.isValid(req.body))) {
      return res.status(422).json({
        message: "Erro de validação. Verifique o body da requisição.",
      });
    }

    try {
      const { pin, register, password } = req.body;

      const user = await User.findOne({ where: { register } });

      if (!user) {
        return res.status(404).json({ message: "Perito não encontrado." });
      }

      if (pin !== user.pin) {
        return res.status(404).json({ message: "PIN incorreto." });
      }

      const password_hash = await bcrypt.hash(password, 8);

      await User.update({ password_hash }, { where: { register } });

      return res.json({ message: "password cadastrada com sucesso." });
    } catch (err) {
      writeLog(err);
      return res.status(500).json({ message: `${err}` });
    }
  }
}

export default new ChangePasswordService();

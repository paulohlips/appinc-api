import Pop from "../models/Pop";
import writeLog from "../../utils/logger";

class ChangePasswordService {
  async index(req, res) {
    try {
      const response = await Pop.findAll({
        attributes: ["id", "name", "area", "class", "subclass", "version"],
      });

      return res.json(response);
    } catch (err) {
      writeLog(err);
      return res.status(500).json({ message: `${err}` });
    }
  }
}

export default new ChangePasswordService();

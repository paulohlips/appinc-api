import User from "../models/User";
import Mail from "../../lib/Mail";
import * as Yup from "yup";
import writeLog from "../../utils/logger";

class SendPinEmailService {
  async store(req, res) {
    const schema = Yup.object().shape({
      register: Yup.number().required(),
    });

    if (!(await schema.isValid(req.body))) {
      return res.status(422).json({
        message: "Erro de validação. Verifique o body da requisição.",
      });
    }

    try {
      const { register } = req.body;

      const user = await User.findOne({ where: { register } });

      if (!user) {
        return res.status(404).json({ message: "Perito não encontrado." });
      }

      const pin = Math.floor(Math.random() * 8999 + 1000);

      await User.update({ pin }, { where: { register } });

      await Mail.sendMail({
        to: `${user.email}`,
        subject: "Código de acesso - AppINC",
        text: `O seu PIN é ${pin}`,
      });

      return res.json({ message: `PIN enviado para o email ${user.email}` });
    } catch (err) {
      writeLog(err);
      return res.status(500).json({ message: `${err}` });
    }
  }
}

export default new SendPinEmailService();

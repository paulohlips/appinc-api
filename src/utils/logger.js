import appRoot from "app-root-path";
import fs from "fs";

const writeLog = (info) => {
  fs.appendFile(
    `${appRoot}/src/logs/error-log.txt`,
    [new Date(), info, "\r\n"],
    function (err) {
      appRoot;
      if (err) return console.log(err);
    }
  );
};

export default writeLog;

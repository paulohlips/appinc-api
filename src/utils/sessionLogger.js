import appRoot from "app-root-path";
import fs from "fs";

const writeLog = (id, name, email) => {
  fs.appendFile(
    `${appRoot}/src/logs/sessions-log.txt`,
    [new Date(), id, name, email, "\r\n"],
    function (err) {
      appRoot;
      if (err) return console.log(err);
    }
  );
};

export default writeLog;

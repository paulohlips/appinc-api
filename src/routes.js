import { Router } from "express";
import cors from "cors";

import UserController from "./app/controllers/UserController";

import SendPinEmailService from "./app/services/SendPinEmailService";
import ChangePasswordService from "./app/services/ChangePasswordService";
import SendHierarchy from "./app/services/SendHierarchy";
import PersistExamDataService from "./app/services/PersistExamDataService";

import SessionController from "./app/controllers/SessionController";
import PopController from "./app/controllers/PopController";

import authMiddleware from "./app/middlewares/auth";

const routes = new Router();

routes.use(cors());

routes.get("/", (req, res) => {
  return res.json({ message: "Fala deeeev, estamos online! " });
});

routes.post("/sessions", SessionController.store);

routes.post("/register", SendPinEmailService.store);
routes.post("/changepassword", ChangePasswordService.store);

routes.post("/pops", PopController.store);
routes.get("/pops/:id", PopController.show);

routes.get("/hierarchy", SendHierarchy.index);

routes.post("/exams/:exam_type", PersistExamDataService.store);

export default routes;

exports.up = (knex) =>
  knex.schema.createTable("pops", (table) => {
    table.increments("id");

    table.string("name").notNullable();
    table.string("version").notNullable();
    table.string("area").notNullable();
    table.string("class").notNullable();
    table.string("subclass").notNullable();
    table.text("details");
    table.boolean("status").defaultTo(true);
    table.specificType("data", "json[]").notNullable();
  });

exports.down = (knex) => knex.schema.dropTable("pops");

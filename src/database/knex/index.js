import knex from "knex";
import conf from "../../../knexfile";

const connection = knex(conf.development);

export default connection;
